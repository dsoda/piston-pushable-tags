package net.dsoda.pistonpushabletags.mixin;

import net.dsoda.pistonpushabletags.PistonPushableTags;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FacingBlock;
import net.minecraft.block.PistonBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PistonBlock.class)
public abstract class PistonBlockMixin extends FacingBlock {

    protected PistonBlockMixin(Settings settings) {
        super(settings);
    }

    /**
     * @author dsoda
     * @reason testing!
     */
    @Overwrite
    public static boolean isMovable(BlockState state, World world, BlockPos pos, Direction direction, boolean canBreak, Direction pistonDir) {
        if (pos.getY() < world.getBottomY() || pos.getY() > world.getTopY() - 1 || !world.getWorldBorder().contains(pos))
            return false;
        if (state.isAir())
            return true;
        if (direction == Direction.DOWN && pos.getY() == world.getBottomY())
            return false;
        if (direction == Direction.UP && pos.getY() == world.getTopY() - 1)
            return false;
        if (state.isOf(Blocks.PISTON) || state.isOf(Blocks.STICKY_PISTON)) {
            if (state.get(PistonBlock.EXTENDED))
                return false;
        } else {
            switch (state.getPistonBehavior()) {
                case BLOCK: {
                    return false;
                }
                case DESTROY: {
                    return canBreak;
                }
                case PUSH_ONLY: {
                    return direction == pistonDir;
                }
            }
        }
        return !state.hasBlockEntity();
    }
}
