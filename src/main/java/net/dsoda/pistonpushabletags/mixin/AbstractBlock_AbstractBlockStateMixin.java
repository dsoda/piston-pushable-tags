package net.dsoda.pistonpushabletags.mixin;

import net.dsoda.pistonpushabletags.PistonPushableTags;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.piston.PistonBehavior;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(AbstractBlock.AbstractBlockState.class)
public abstract class AbstractBlock_AbstractBlockStateMixin {

    @Inject(method = "getPistonBehavior", at = @At(value = "HEAD"), cancellable = true)
    private void getNewPistonBehavior(CallbackInfoReturnable<PistonBehavior> cir) {
        AbstractBlock.AbstractBlockState state = (AbstractBlock.AbstractBlockState)(Object)(this);
        if (state.isIn(PistonPushableTags.PUSHABLE)) cir.setReturnValue(PistonBehavior.NORMAL);
        else if (state.isIn(PistonPushableTags.NOT_PUSHABLE)) cir.setReturnValue(PistonBehavior.BLOCK);
        else if (state.isIn(PistonPushableTags.BREAK)) cir.setReturnValue(PistonBehavior.DESTROY);
        else if (state.isIn(PistonPushableTags.PUSH_ONLY)) cir.setReturnValue(PistonBehavior.PUSH_ONLY);
        //else if (state.hasBlockEntity()) cir.setReturnValue(PistonBehavior.BLOCK);
    }
}
