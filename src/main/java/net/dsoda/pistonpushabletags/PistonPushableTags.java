package net.dsoda.pistonpushabletags;

import net.fabricmc.api.ModInitializer;

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricTagProvider;
import net.minecraft.block.Block;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;

public class PistonPushableTags implements ModInitializer {

	public static final String MOD_ID = "pistonpushabletags";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	public static final TagKey<Block> PUSH_ONLY = TagKey.of(RegistryKeys.BLOCK, Identifier.of(MOD_ID, "piston_push_only"));
	public static final TagKey<Block> PUSHABLE = TagKey.of(RegistryKeys.BLOCK, Identifier.of(MOD_ID, "piston_pushable"));
	public static final TagKey<Block> NOT_PUSHABLE = TagKey.of(RegistryKeys.BLOCK, Identifier.of(MOD_ID, "piston_not_pushable"));
	public static final TagKey<Block> BREAK = TagKey.of(RegistryKeys.BLOCK, Identifier.of(MOD_ID, "piston_break"));

	@Override
	public void onInitialize() {
		LOGGER.info("Initializing \"Piston Pushable Tags\"");
	}

	public static class BlockTagProvider extends FabricTagProvider.BlockTagProvider {

		public BlockTagProvider(FabricDataOutput output, CompletableFuture<RegistryWrapper.WrapperLookup> registriesFuture) {
			super(output, registriesFuture);
		}

		@Override
		public void configure(RegistryWrapper.WrapperLookup arg) {
			getOrCreateTagBuilder(PUSH_ONLY)
			;
			getOrCreateTagBuilder(NOT_PUSHABLE)
			;
			getOrCreateTagBuilder(BREAK)
			;
		}
	}
}